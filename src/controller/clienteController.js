const connect = require("../db/connect");

module.exports = class clienteController {
  static async createCliente(req, res) {
    const {
      telefone,
      nome,
      cpf,
      logradouro,
      numero,
      complemento,
      bairro,
      cidade,
      estado,
      cep,
      referencia,
    } = req.body;

    if (telefone !== 0) {
      const query = `INSERT INTO cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) VALUES (
        '${telefone}',
        '${nome}',
        '${cpf}',
        '${logradouro}',
        '${numero}',
        '${complemento}',
        '${bairro}',
        '${cidade}',
        '${estado}',
        '${cep}',
        '${referencia}'
      )`;

      try {
        connect.query(query, function (err) {
          if (err) {
            console.log(err);
            res.status(500).json({ error: "Usuário não cadastrado no banco" });
            return;
          }
          console.log("Inserido no banco");
          res.status(201).json({ message: "Usuário criado com sucesso" });
        });
      } catch (error) {
        console.error("Erro ao executar o insert", error);
        res.status(500).json({ error: "Erro interno do servidor" });
      }//Fim do catch
    } //Fim do if
    else {
      res.status(400).json({ message: "Número de telefone inválido" });
    }
  } // Fim do createCliente

  static async getClientes(req, res) {
    const query = `SELECT * FROM cliente`;

    try {
      connect.query(query, function (err, rows) {
        if (err) {
          console.log(err);
          res.status(500).json({ error: "Erro ao buscar clientes no banco de dados" });
          return;
        }
        console.log("Clientes recuperados do banco");
        res.status(200).json({ data: rows });
      });
    } catch (error) {
      console.error("Erro ao executar a consulta", error);
      res.status(500).json({ error: "Erro interno do servidor" });
    }
  }
};
