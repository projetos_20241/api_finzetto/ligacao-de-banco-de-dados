const connect = require("../db/connect")


module.exports = class dbController{
    static async getTables(req, res) {
        
        const queryShowTables = "SHOW TABLES";
    
        connect.query(queryShowTables, function(err, result, fields) {
            if (err) {
                console.log('Erro ao obter as tabelas:', err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
            }
    
            const tableNames = result.map(row => row[Object.keys(row)[0]]);
            res.status(200).json({ tables: tableNames });
        });
    }

    static async getTablesDescriptions(req, res) {
        
        const queryDescTables = "SHOW TABLES";
    
        connect.query(queryDescTables, async function(err, result, fields) {
            if (err) {
                console.log('Erro ao obter as tabelas:', err);
                return res.status(500).json({ error: "Erro ao obter tabelas do banco de dados" });
            }
    
            const tables = [];
    
            for (let i = 0; i < result.length; i++) {
                const tableName = result[i][Object.keys(result[i])[0]];
                const queryDescTable = `DESCRIBE ${tableName}`;
    
                try {
                    const tableDescription = await new Promise((resolve, reject) => {
                        connect.query(queryDescTable, function(err, result, fields) {
                            if (err) {
                                reject(err);
                            }
                            resolve(result);
                        });
                    });
                    tables.push({ name: tableName, description: tableDescription });
                } catch (error) {
                    console.log(error);
                    return res.status(500).json({ error: "Erro ao obter a descrição da tabela!" });
                }
            }
    
            res.status(200).json({ tables });
        });
    }
    
}