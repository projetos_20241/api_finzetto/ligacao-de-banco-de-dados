const dbController = require('../controller/dbController')
const clienteController = require('../controller/clienteController')
const router = require('express').Router()

router.get("/tables", dbController.getTables)
router.get("/tablesDescription", dbController.getTablesDescriptions)
router.get("/cliente", clienteController.getClientes)
router.post("/cliente", clienteController.createCliente)
module.exports = router